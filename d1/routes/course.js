const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// Route for creating a course
/*
router.post("/", auth.verify, (req, res) => {

	const courseData = auth.decode(req.headers.authorization);

	courseController
	.addCourse({ admin: courseData.isAdmin }, req.body)
	.then(resultFromController => res.send(resultFromController))
})
*/

router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})


// Retrieve all Courses
router.get("/all", (req, res) => {
	courseController.getAllCourses()
	.then(resultFromController => res.send(resultFromController));
})


// Retrieve all active courses
router.get("/", (req, res) => {
	courseController.getAllActive()
	.then(resultFromController => res.send(resultFromController))
});



// Retrieve a specific Course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Update a Course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body)
	.then(resultFromController => res.send(resultFromController));
})

// Archiving an Inactive Course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body)
	.then(resultFromController => res.send(resultFromController));
})



module.exports = router;