// JSON Web Tokens are standard for sending info between our app in a secure manner
// This will allow us to gain access to methods that will help us to create a JSON web token

const jwt = require('jsonwebtoken');

const secret = "CrushAkoNgCrushKo";

/*	Process

		JWT is a way of securely passing info from the server to the front-end or to the other parts of server

		- info is kept secure throught the use of the secret code
		- only the system that knows the secret code that can decode the encrypted info
*/


// Token Creation 
/* 
	Analogy:
		- Pack the gift and provide a lock with the secret code as the key
*/

module.exports.createAccessToken = (user) => {
	// Data wil be received from the registration form
	// When the users log in , a token wil be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
}



///////// December 9, 2021

/* 
	Authorization
		- The process of determining user access to resources or functionalities

	REST API's are "stateless", hence authorization has to be implemented via the use of JSON Web Tokens (JWT's)

	Workflow:
		1. User logs in, server will respond w/ JWT on successful authentication
		2. JWT will be included in the header of the GET request to the /users/details endpoint
		3. Server validates JWT
			a. if valid, user details will be sent in the response
			b. if invalid, DENY request

*/

// Token Verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if (typeof token !== "undefined") {
		console.log(token);

		// "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxYWYwMWRlY2FhOWY4MTU1MWY4YTEzYSIsImVtYWlsIjoiY2FybGRvZUBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzkwMTA2NTF9.RNtITDtdRpiBqUi-X7GODrOU7OXsdM4Hirr3MEXN1nc"
		// for us not to include the word "Bearer " we need to slice it down
		// syntax: .slice(<number of index>, <variable the contains the string>)
		token = token.slice(7, token.length)


		// Validate the Token using the "verify" Method
		return jwt.verify(token, secret, (err, data) => {
			// If JWT is not VALID
			if (err) {
				return res.send({ auth: "failed" });
			}
			else {
				// allows the application to proceed with the next middleware function/callback function in te route
				// if verified next() acts like a "guard" that lets the user "enter" once verified
				next();
			}
		})
	}
	// Token does not exist
	else {
		return res.send({ auth: "failed" })
	}
}

// Token Decryption

module.exports.decode = (token) => {
	// Token received and is not undefined
	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null;
			}
			else {
				// ".decode" Method is used to obtain the information from the JWT
				// "{complete: true}" Option will allows to return addtional information from the JWT
				// "payload" stores the information that was provided 
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated.

				return jwt.decode(token, { complete: true }).payload
			}
		})
	}
	else {
		return null;
	}
}