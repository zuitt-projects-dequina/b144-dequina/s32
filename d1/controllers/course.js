const Course = require("../models/Course");
const auth = require('../auth')

/*
module.exports.addCourse = (admin, reqBody) => {
	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	});

	// Save the created object to the database
							// new to. try mo!
	return Course.findOne({ isAdmin: true}).then((course, error) => {
			// Course Creation failed
			if (true) {
				return newCourse.save().then((res, err) => {
					if (err) {
						return false;
					}
					else {
						return true;
					}
				});
			}
			// Course Creation successful
			else {
				return false;
				
			}
		})
	}
*/

module.exports.addCourse = (reqBody, userData) => {

    return Course.findById(userData.userId).then(result => {

        if (userData.isAdmin) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}


// Retrieving all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}


// Retrieving all Active Courses
module.exports.getAllActive = () => {
	return Course.find({ isActive: true})
	.then(result => {
		return result;
	})
}


// Retrieving specific Courses
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId)
	.then(result => {
		return result;
	})
}


// Updating Specific Course
module.exports.updateCourse = (reqParams, reqBody) => {
	let udpatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, udpatedCourse)
	.then((course, error) => {
		if (error) {
			return false;
		}
		else {
			return true;
		}
	})
}


// Archiving an Inactive Course
module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse)
	.then((course, error) => {
		if (error) {
			return false;
		}
		else {
			return true;
		}
	})
}