const User = require('../models/User');
const Course= require('../models/Course')
const bcrypt = require('bcrypt');
const auth = require('../auth')

/*
	Business Logic 
	Checking if the email already exists
		1. Use find() Method to find duplicate emails
		2. Error handling, if no duplicate found, return false, else, return true

	IMPORTANT NOTE:
		best practice to return a result is to use a "Boolean" or return an "object/array" of object. Because string is limited in our backend, and cannot be connected to out front-end

*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		}
		else {
			// no duplicate email found
			return false;
		}
	})
}


// User Registration
/*
	Steps:
		1. Create a new user object using the mongoose model and from the info from the request body
		2. Make sure that the password is encrypted
		3. Save the new user to the database
*/

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		// 10 is the is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
		isAdmin: reqBody.isAdmin
	})

	// save
	return newUser.save().then((user, error) => {
		// if registration failed
		if (error) {
			return false;
		}
		else {
			// user registration is successful
			return true;
		}
	})
}

/* USER AUTHENTICATION

	Steps:
		1. Check if the user email exist in our database
		2. If the user exists, Compare the password provided in the login form with the password stored in the database
		3. Generate/Return a JSON web token if the use is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	// findOne() Method returnd the first record in the collection that matches the search criteria
	// We use findOne() instead of 'find()' method which returns all records that match the seach criteria
	return User.findOne({ email: reqBody.email }).then(result => {
		// User does not exist
		if (result == null) {
			return false;
		}
		else {
			// User exists
			// The "compareSync" method is used to compare a non-encrypted password from the lgoin form to the encrypted password retrieved from the database and returns 'true' or 'false' value depending on the result.
			 const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			 // if the password match/result of the above code is true
			 if (isPasswordCorrect) {
			 	// generate access token
			 	// The mongoose toObject method convert the mongoose object into a plain javascript object
			 	// this is an object, hence {} is used
			 	return { accessToken: auth.createAccessToken(result.toObject())}
			 }
			 else {
			 	// password do not match
			 	return false;
			 }
		}
	})
}


//////// ACTIVITY ///////////

// getting user details without the password
/*
module.exports.userDetails = (reqBody) => {
	return User.findById(reqBody, { password: 0 }).then(result => {
		if (result == null) {
			return false;
		}
		else {
			return result;
		}
	})

}


// getting user details: password showing empty string
module.exports.userDetails = (reqBody) => {
	return User.findById(reqBody).then(result => {
		if (result == null) {
			return false;
		}
		else {
			let details = new User({
				firstName: result.firstName,
				lastName: result.lastName,
				age: result.age,
				gender: result.gender,
				email: result.email,
				password: "",
				mobileNo: result.mobileNo
			})
			return details;
		}
	})

}
*/

// Solution for S33 Activity
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application

		result.password = "";

		// Returns the user information with the password as an empty string

		return result;

	});

};




// Enroll user to a class
/* Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollement array
	3. Update the document in the MongoDB Atlas Databas
*/

module.exports.enroll = async (data) => {

	// Add the course ID in the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {

		// Adds the courseId in the user's enrollments array
		user.enrollments.push({ courseId: data.courseId })

		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		})

	})

	// Add the user ID in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		//adds the userId in the courses's enrolless array
		course.enrollees.push({ userId: data.userId })

		// Saves the updated course information in the database
		return course.save().then((course,error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		})
	})

	// Condition that will check if the user and course documents have been updated
	if (isUserUpdated && isCourseUpdated) {
		return true;
	}
	else {
		return false;
	}
}